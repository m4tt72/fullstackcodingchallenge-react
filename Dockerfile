FROM node:10.15.3-alpine
WORKDIR /usr/src/express-ts-rest-api
COPY package.json ./
RUN npm install
COPY . .
