# Fullstack coding challenge

## Tasks

**Done**

* As a User, I can sign up using my email & password 
* As a User, I can sign in using my email & password 
* As a User, I can display the list of shops sorted by distance 
* As a User, I can like a shop, so it can be added to my preferred shops
* Acceptance criteria: liked shops shouldn’t be displayed on the main page
* As a User, I can display the list of preferred shops
* As a User, I can remove a shop from my preferred shops list

**Skipped**

* As a User, I can dislike a shop, so it won’t be displayed within “Nearby Shops” list during the next 2 hours 

## Requirements

* Docker
* Nodejs (10+)

## Getting started

### Run backend with docker

```bash
$ docker-compose up --build
```

This will run the backend project in development mode (Watch mode) on <http://localhost:3001>.

Any changes made in the project will trigger a hot reload!

## Run frontend

### Installing dependencies

```bash
$ cd frontend
$ npm install 
```

### Running React dev server

```bash
$ npm start
```

# Libraries

For the backend I used my own express ts boilerplate: [express-ts-rest-api](https://github.com/m4tt72/express-ts-rest-api)
