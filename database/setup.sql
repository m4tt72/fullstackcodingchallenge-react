-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.25 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for codingchallengedb
CREATE DATABASE IF NOT EXISTS `codingchallengedb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `codingchallengedb`;

-- Dumping structure for table codingchallengedb.DislikedShops
CREATE TABLE IF NOT EXISTS `DislikedShops` (
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `disliked_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`shop_id`),
  KEY `shop_id` (`shop_id`),
  CONSTRAINT `DislikedShops_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`),
  CONSTRAINT `DislikedShops_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `Shops` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table codingchallengedb.DislikedShops: ~0 rows (approximately)
/*!40000 ALTER TABLE `DislikedShops` DISABLE KEYS */;
/*!40000 ALTER TABLE `DislikedShops` ENABLE KEYS */;

-- Dumping structure for table codingchallengedb.LikedShops
CREATE TABLE IF NOT EXISTS `LikedShops` (
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`shop_id`),
  KEY `shop_id` (`shop_id`),
  CONSTRAINT `LikedShops_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`),
  CONSTRAINT `LikedShops_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `Shops` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table codingchallengedb.LikedShops: ~0 rows (approximately)
/*!40000 ALTER TABLE `LikedShops` DISABLE KEYS */;
/*!40000 ALTER TABLE `LikedShops` ENABLE KEYS */;

-- Dumping structure for table codingchallengedb.Shops
CREATE TABLE IF NOT EXISTS `Shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table codingchallengedb.Shops: ~0 rows (approximately)
/*!40000 ALTER TABLE `Shops` DISABLE KEYS */;
INSERT INTO `Shops` (`id`, `name`) VALUES
	(1, 'shop 1'),
	(2, 'shop 2'),
	(3, 'shop 3'),
	(4, 'shop 4'),
	(5, 'shop 5'),
	(6, 'shop 6'),
	(7, 'shop 7'),
  (8, 'shop 8'),
  (9, 'shop 9'),
  (10, 'shop 10');
/*!40000 ALTER TABLE `Shops` ENABLE KEYS */;

-- Dumping structure for table codingchallengedb.Users
CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(80) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

