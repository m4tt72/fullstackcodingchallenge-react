# Fullstack Coding Challenge Front-End using React

## Getting started

### Installing dependencies

```bash
$ npm install
```

### Running React dev server

```bash
$ npm start
```
