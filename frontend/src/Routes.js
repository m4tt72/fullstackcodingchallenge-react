import React,{ Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Navbar from '../src/components/navbar/Navbar';
import Home from '../src/components/home/Home';
import Register from '../src/components/register/Register';
import Login from '../src/components/login/Login';
import Shops from '../src/components/shops/Shops';
import LikedShops from '../src/components/liked-shops/LikedShops';
import NotFound from '../src/components/not-found/NotFound';

class Routes extends Component {

  render() {
    return (
      <div>
        <Navbar/>
        <Switch>
          <Route path='/' component={Home} exact />
          <Route path='/register' component={Register} />
          <Route path='/login' component={Login} />
          <Route path='/shops' component={Shops} />
          <Route path='/liked' component={LikedShops}/>
          <Route component={NotFound} />
        </Switch>
      </div>
    );
  }

}

export default Routes;
