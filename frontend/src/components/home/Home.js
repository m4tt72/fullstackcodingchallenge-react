import React from 'react';
import Login from '../login/Login';
import UserProvider from './../../providers/UserProvider';
  
class Home extends React.Component {

  state = {
    user: UserProvider.getCurrentUser()
  }
  
  render() {
    if(this.state.user !== null) {
      this.props.history.push('/shops');
      return;
    }
    return (
      <div className="home container mt-5">
        <h1 className="text-center">Welcome to the best Shops app</h1>
        <hr/>
        <Login history={this.props.history} />
      </div>
    );
  }

}

export default Home;
