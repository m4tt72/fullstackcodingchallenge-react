import React, { Component } from 'react';
import UserProvider from '../../providers/UserProvider';
import ShopProvider from '../../providers/ShopProvider';
import Shop from './components/Shop';

class LikedShops extends Component {
  
  state = {
    shops: [],
    user: UserProvider.getCurrentUser()
  };

  async componentDidMount() {
    this.getLikedShops();
  }

  getLikedShops = async  () => {
    try {
      const shops = await ShopProvider.getLiked();
      this.setState({
        shops
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const emptyShops = this.state.shops.length === 0 ? (
      <div className="text-center">
        <h1>No Shops Liked</h1>
      </div>
    ) : (
        <div className="row">
          {
            this.state.shops.map((shop) => <Shop key={shop.id} shop={shop} getLikedShops={this.getLikedShops}/>)
          }
        </div>
      );
    return (
      <div>
        <div className="container p-5">
          <h1>Preffered Shops</h1>
          <hr />
          {emptyShops}
        </div>
      </div>
    );
  }
}

export default LikedShops;
