import React, { Component } from 'react';
import ShopProvider from '../../../providers/ShopProvider';
import './Shop.css';

class Shop extends Component {

  remove = async () => {
    console.log(this.props.shop);
    try {
      ShopProvider.remove(this.props.shop.id);
      this.props.getLikedShops();
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div className="col m-2">
        <div className="card">
          <img src="https://via.placeholder.com/150" className="card-img-top" alt="shop"></img>
          <div className="card-body">
            <h5 className="card-title">{this.props.shop.name}</h5>
            <button className="btn btn-danger" onClick={() => this.remove()}>Remove</button>
          </div>
        </div>
      </div>
    )
  }

}

export default Shop;
