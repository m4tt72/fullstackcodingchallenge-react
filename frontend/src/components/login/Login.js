import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Form from './components/Form';

class Login extends Component {

  render() {
    return (
      <div className=".login">
        <h3>Please login:</h3>
        <Form history={this.props.history} />
        <br />
        Don't have an account yet? <NavLink to="/register">Register</NavLink>
      </div>
    );
  }

}

export default Login;
