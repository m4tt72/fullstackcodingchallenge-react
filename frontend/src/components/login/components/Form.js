import React, { Component } from 'react';
import UserProvider from '../../../providers/UserProvider';

class Form extends Component {

  state = {
    email: '',
    password: ''
  };

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  login = async (e) => {
    e.preventDefault();
    try {
      const user = await UserProvider.login(this.state.email, this.state.password);
      console.log(user);
      this.props.history.push('/shops');
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <form onSubmit={this.login}>
        <div className="form-group">
          <label>Email</label>
          <input
            className="form-control"
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.onChange}
          />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.onChange}
          />
        </div>
        <input className="btn btn-primary" type="submit" value="Login" />
      </form>
    );
  }

}

export default Form;
