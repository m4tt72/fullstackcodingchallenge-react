import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import UserProvider from '../../providers/UserProvider';

class Navbar extends Component {

  state = {
    user: UserProvider.getCurrentUser()
  }

  logout = () => {
    UserProvider.logout();
  }

  render() {
    console.log(this.props.user);
    const links = this.state.user ? (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <NavLink to="/shops" className="nav-link">Nearby shops</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/liked" className="nav-link">My Preffered Shops</NavLink>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {this.state.user.email.split('@')[0]}
          </a>
          <div className="dropdown-menu" aria-labelledby="navbarDropdown">
            <NavLink  className="dropdown-item" to={'/'} onClick={this.logout}>Logout</NavLink>
          </div>
        </li>
      </ul>
    ) : null;

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <NavLink to={'/'} className="navbar-brand">Coding Challenge</NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {links}
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;
