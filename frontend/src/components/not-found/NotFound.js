import React, { Component } from 'react';

class NotFound extends Component {
  state = {  }
  render() {
    return (
      <div className="m-5 p-5 text-center">
        <h1>404 - Not Found</h1>
      </div>
    );
  }
}

export default NotFound;
