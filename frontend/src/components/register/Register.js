import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Form from './components/Form';
import UserProvider from './../../providers/UserProvider';

class Register extends Component {

  render() {
    if (UserProvider.getCurrentUser()) {
      this.props.history.push('/shops');
      return;
    }
    return (
      <div className="container mt-5">
        <h3>Create an account</h3>
        <Form history={this.props.history} />
        <br />
        Already have an account? <NavLink to="/">Login</NavLink>
      </div>
    );
  }
}

export default Register;
