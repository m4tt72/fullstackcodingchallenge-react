import React, { Component } from 'react';
import UserProvider from '../../../providers/UserProvider';

class Form extends Component {

  state = {
    email: '',
    password: '',
    passwordRepeat: '',
    error: null
  };

  onChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  login = async (e) => {
    e.preventDefault();
    if (this.state.password !== this.state.passwordRepeat) {
      this.setState({
        error: 'Password mismatch! Please verify you entered the same password'
      });
      return;
    } 
    this.setState({
      errors: []
    });
    try {
      const user = await UserProvider.register(this.state.email, this.state.password);
      console.log(user);
      this.setState({
        error: null
      });
      this.props.history.push('/shops');
    } catch (error) {
      console.log(error);
      this.setState({
        error: 'Registration failed: ' + error.response.data.message
      });
    }
  }

  render() {
    const alert = this.state.error ? 
    (
      <div className="alert alert-danger" role="alert">
        {this.state.error}
      </div>
    ): null;
    return (
      <form onSubmit={this.login}>        
        {alert}
        <div className="form-group">
          <label>Email</label>
          <input
            className="form-control"
            type="email"
            name="email"
            value={this.state.email}
            onChange={this.onChange}
          />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input
            className="form-control"
            type="password"
            name="password"
            value={this.state.password}
            onChange={this.onChange}
          />
        </div>
        <div className="form-group">
          <label>Repeat your password</label>
          <input
            className="form-control"
            type="password"
            name="passwordRepeat"
            value={this.state.passwordRepeat}
            onChange={this.onChange}
          />
        </div>
        <input className="btn btn-primary" type="submit" value="Register" />
      </form>
    );
  }

}

export default Form;
