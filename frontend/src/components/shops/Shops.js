import React, { Component } from 'react';
import UserProvider from '../../providers/UserProvider';
import ShopProvider from '../../providers/ShopProvider';
import Shop from './components/Shop';

class Shops extends Component {

  state = {
    user: UserProvider.getCurrentUser(),
    shops: []
  };

  async componentDidMount() {
    this.getShops();
  }

  getShops = async () => {
    try {
      const shops = await ShopProvider.getShops();
      console.log(shops);
      this.setState({
        shops
      });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    if (this.state.user === null) {
      this.props.history.push('/');
      return;
    }
    const shopsList = this.state.shops.length > 0 ? (
      <div className="row">
          {this.state.shops.map((shop) => <Shop key={shop.id} shop={shop} getShops={this.getShops} />)}
      </div>
    ): (
      <h2 className="text-center">No nearby shops found!</h2>
    );
    return (
      <div className="container mt-5">
        <h1>Shops</h1>
        <hr />
        { shopsList }  
      </div>
    );
  }
  
}

export default Shops;
