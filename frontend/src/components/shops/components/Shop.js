import React, { Component } from 'react';
import './Shop.css';
import ShopProvider from '../../../providers/ShopProvider';

class Shop extends Component {

  like = async () => {
    console.log(this.props.shop);
    try {
      ShopProvider.like(this.props.shop.id);
      this.props.getShops();
    } catch (error) {
      console.log(error);
    }
  }

  dislike = async () => {
    console.log(this.props.shop);
    this.props.getShops();
  }

  render() {
    return (
      <div className="col m-2">
        <div className="card">
          <img src="https://via.placeholder.com/150" className="card-img-top" alt="shop"></img>
          <div className="card-body">
            <h5 className="card-title">{this.props.shop.name}</h5>
            <button className="btn btn-primary" onClick={() => this.like()}>Like</button>
            &nbsp;
            <button className="btn btn-danger" onClick={() => this.dislike()}>Dislike</button>
          </div>
        </div>
      </div>
    );
  }

}

export default Shop;
