import axios from 'axios';
import UserProvider from './UserProvider';

class ShopProvider {
  
  baseUrl = 'http://localhost:3001/api/shops';
  currentUser = UserProvider.getCurrentUser();

  async getShops() {
    try {
      const response = await axios.get(`${this.baseUrl}/${this.currentUser.id}`);
      return response.data;
    } catch (error) {
      throw error;
    }
  }

  async getLiked() {
    try {
      const response = await axios.get(`${this.baseUrl}/liked/${this.currentUser.id}`);
      return response.data;
    } catch (error) {
      throw error;
    }
  }

  async like(shop) {
    try {
      const response = await axios.get(`${this.baseUrl}/like/${this.currentUser.id}/${shop}`);
      return response.data;
    } catch (error) {
      throw error;
    }
  }

  async remove(shop) {
    try {
      const response = await axios.get(`${this.baseUrl}/remove/${this.currentUser.id}/${shop}`);
      return response.data;
    } catch (error) {
      throw error;
    }
  }

}

export default new ShopProvider();
