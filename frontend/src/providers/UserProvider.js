import axios from 'axios';

class UserProvider {
  
  baseUrl = 'http://localhost:3001/api/users';

  getCurrentUser = () => {
    return JSON.parse(localStorage.getItem('user'));
  }

  login = async (email, password) => {
    try {
      const response = await axios.post(`${this.baseUrl}/login`, { email, password });
      const user = response.data;
      localStorage.setItem('user', JSON.stringify(user));
      return user;
    } catch (error) {
      throw error;
    }
  }

  register = async (email, password) => {
    try {
      const response = await axios.post(`${this.baseUrl}/register`, { email, password });
      const user = response.data;
      localStorage.setItem('user', JSON.stringify(user));
      return user;
    } catch (error) {
      throw error;
    }
  }

  logout = () => {
    localStorage.removeItem('user');
  }

}

export default new UserProvider();
