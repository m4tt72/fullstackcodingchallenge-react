import { createServer } from 'http';
import app from './src/app';

const server = createServer(app);

server.listen(+process.env.PORT | 3000, () => {
    console.log(`[!] Server started on http://localhost:${+process.env.PORT | 3000}`);
});
