import * as Express from 'express';
import * as Cors from 'cors';
import * as Helmet from 'helmet';
import * as Logger from 'morgan';
import * as Compress from 'compression';
import { json } from 'body-parser';
import Router from './router';

class App {

    public app: Express.Application;
    
    constructor() {
        this.app = Express();
        this.init();
        this.app.use('/api', Router);
    }

    private init() {
        this.app.use(
            Logger('combined'),
            Cors({
                origin: process.env.NODE_ENV === 'production' ? [ '' ] : '*'
            }),
            Helmet(),
            json(),
            Compress()
        );
    }

}

export default new App().app
