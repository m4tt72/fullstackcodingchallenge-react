import { createConnection, MysqlError } from 'mysql';

const connection = createConnection({
    host: process.env.DB_HOST,
    port: +process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASS
});

connection.on('error', (error: MysqlError) => {
    console.log(error);
});

export default connection;

