import { Request, Response } from 'express';
import AuthController from './../controllers/auth.controller';
import ShopsController from './../controllers/shops.controller';

export const routes: Route[] = [
    {
        path: '/users/register',
        method: 'post',
        middlewares: [],
        controller: AuthController.register
    },
    {
        path: '/users/login',
        method: 'post',
        middlewares: [],
        controller: AuthController.login
    },
    {
        path: '/shops/:user',
        method: 'get',
        middlewares: [],
        controller: ShopsController.getShops
    },
    {
        path: '/shops/liked/:user',
        method: 'get',
        middlewares: [],
        controller: ShopsController.getLiked
    },
    {
        path: '/shops/like/:user/:shop',
        method: 'get',
        middlewares: [],
        controller: ShopsController.like
    },
    {
        path: '/shops/remove/:user/:shop',
        method: 'get',
        middlewares: [],
        controller: ShopsController.remove
    },
    {
        path: '*',
        method: 'all',
        middlewares: [],
        controller: (req: Request, res: Response) => {
            res.status(404).json({
                message: 'No path found!'
            });
        }
    }
];

interface Route {
    path: string;
    method: 'get' | 'post' | 'put' | 'delete' | 'all';
    middlewares: any[];
    controller: any;
}
