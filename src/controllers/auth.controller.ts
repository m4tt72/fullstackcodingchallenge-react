import { Request, Response } from 'express';
import { compare } from 'bcryptjs';
import UserModel from '../models/user.model';

class AuthController {

    public async login(req: Request, res: Response) {
        const { email, password } = req.body;
        let user;
        try {
            user = await UserModel.findByEmail(email);
        } catch (error) {
            return res.status(404).json(error);
        }
        try {
            const isValid = await compare(password, user.password);
            if(!isValid) {
                return res.status(401).json({
                    message: 'Wrong password'
                });
            }
            return res.status(200).json(user);
        } catch (error) {
            return res.status(400).json(error);
        }
    }

    public async register(req: Request, res: Response) {
        const { email, password } = req.body;
        try {
            const result = await UserModel.create(email, password);
            return res.status(200).json(result);
        } catch (error) {
            if(error )
            console.log(error);
            return res.status(400).json({
                message: error.sqlMessage
            });
        }
    }
    
}

export default new AuthController();
