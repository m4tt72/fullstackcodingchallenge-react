import { Request, Response } from "express";
import ShopModel from './../models/shop.model';
import UserModel from './../models/user.model';

class ShopsController {

  public async getShops(req: Request, res: Response) {
    const { user } = req.params;
    try {
      const shops = await ShopModel.findAll(user);
      return res.status(200).json(shops);
    } catch (error) {
      return res.status(400).json(error);
    }
  }

  public async getLiked(req: Request, res: Response) {
    const { user } = req.params;
    try {
      const shops = await UserModel.likedShops(+user);
      return res.status(200).json(shops);
    } catch (error) {
      return res.status(400).json(error);
    }
  }

  public async like(req: Request, res: Response) {
    const { user, shop } = req.params;
    try {
      const shops = await UserModel.likeShop(user, shop);
      return res.status(200).json(shops);
    } catch (error) {
      return res.status(400).json(error);
    }
  }

  public async remove(req: Request, res: Response) {
    const { user, shop } = req.params;
    try {
      const shops = await UserModel.removeShopFromLiked(user, shop);
      return res.status(200).json(shops);
    } catch (error) {
      return res.status(400).json(error);
    }
  }

}

export default new ShopsController();
