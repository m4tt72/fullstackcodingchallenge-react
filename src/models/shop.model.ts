import Database from './../config/database';
import { MysqlError, FieldInfo } from 'mysql';

/**
CREATE OR RAPLACE TABLE Users (
	id INT PRIMARY KEY AUTO_INCREMENT,
	email VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(80) NOT NULL,
	created_at DATETIME,
	updated_at datetime
);
 */
export interface User {
    id?: number;
    email: string;
    password: string;
    created_at: Date;
    updated_at: Date;
}

class UserModel {

    public async findAll(user: number): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            const query = "SELECT * FROM Shops AS s WHERE id NOT IN (SELECT shop_id FROM LikedShops as ls WHERE ls.user_id=? AND ls.shop_id=s.id)";
            Database.query(
                query,
                [ user ],
                (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if(err) reject(err);
                    resolve(result);
                }
            );
        });
    }

}

export default new UserModel();
