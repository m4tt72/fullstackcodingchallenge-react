import Database from './../config/database';
import { MysqlError, FieldInfo } from 'mysql';
import { hash } from 'bcryptjs';
import ShopModel from './shop.model';

export interface User {
    id?: number;
    email: string;
    password: string;
    created_at: Date;
    updated_at: Date;
}

class UserModel {

    public async create(email: string, password: string): Promise<User> {
        return new Promise<User>(async (resolve, reject) => {
            const query = 'INSERT INTO Users(email, password, created_at, updated_at) VALUES (?,?,?,?)';
            try {
                password = await hash(password, 10);
            } catch (error) {
                reject(error);
            }
            Database.query(
                query,
                [email, password, new Date(), new Date()],
                async (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if (err) reject(err);
                    try {
                        const user = await this.findByEmail(email);
                        resolve(user);
                    } catch (error) {
                        reject(error);
                    }
                }
            )
        });
    }

    public async findByEmail(email: string): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            const query = "SELECT * FROM Users WHERE email=?";
            Database.query(
                query,
                [email],
                (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if (err) reject(err);
                    if (result) {
                        resolve(result[0]);
                    }
                    reject('No User Found');
                }
            )
        });
    }

    public async likedShops(id: number): Promise<User> {
        return new Promise<User>((resolve, reject) => {
            const query = "SELECT * FROM Shops WHERE Shops.id IN (SELECT shop_id FROM LikedShops WHERE user_id=?)";
            Database.query(
                query,
                [id],
                (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if (err) reject(err);
                    resolve(result);
                }
            )
        });
    }

    public likeShop(user: number, shop: number) {
        return new Promise<User>((resolve, reject) => {
            const query = "INSERT INTO LikedShops(user_id, shop_id) VALUES(?,?)";
            Database.query(
                query,
                [user, shop],
                async (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if (err) reject(err);
                    try {
                        const shops = await ShopModel.findAll(user);
                        resolve(shops);
                    } catch (error) {
                        reject(error);
                    }
                }
            )
        });
    }

    public removeShopFromLiked(user: number, shop: number) {
        return new Promise<User>((resolve, reject) => {
            const query = "DELETE FROM LikedShops WHERE user_id=? AND shop_id=?";
            Database.query(
                query,
                [user, shop],
                async (err: MysqlError, result: any, fields: FieldInfo[]) => {
                    if (err) reject(err);
                    try {
                        const shops = await this.likedShops(user);
                        resolve(shops);
                    } catch (error) {
                        reject(error);
                    }
                }
            )
        });
    }

}

export default new UserModel();
