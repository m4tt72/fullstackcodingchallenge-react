import { Router as ExpressRouter } from 'express';
import { routes } from './config/routes';

class Router {

    public router: ExpressRouter;

    constructor() {
        this.router = ExpressRouter();
        this.init();
    }

    private init() {
        for(const route of routes) {
            route.middlewares.push(route.controller);
            this.router[route.method](route.path, ...route.middlewares);
        }
    }
}

export default new Router().router;
